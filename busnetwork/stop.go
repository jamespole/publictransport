package busnetwork

// Stop describes a stop.
type Stop struct {
	ID         int
	Name       string
	Services   Services
	StopRoutes StopRoutes
}

// Stops is a collection of pointers to Stop(s).
type Stops []*Stop

// GetByID returns the stop with the given id within stops. Returns nil if the
// stop was not found.
func (stops Stops) GetByID(id int) *Stop {
	for _, stop := range stops {
		if stop.ID == id {
			return stop
		}
	}
	return nil
}
