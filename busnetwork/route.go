package busnetwork

// Route describes a route.
type Route struct {
	ID         int
	LongName   string
	ShortName  string
	StopRoutes StopRoutes
}

// Routes is a collection of pointers to Route(s).
type Routes []*Route

// GetByID returns the route with the given id within routes.
func (routes Routes) GetByID(id int) *Route {
	for _, route := range routes {
		if route.ID == id {
			return route
		}
	}
	return nil
}
