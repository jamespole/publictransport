package busnetwork

import "testing"

func TestGetRouteByID(t *testing.T) {
	var testID = 1234
	var route = Route{
		ID: testID,
	}
	var routes = Routes{}
	routes = append(routes, &route)
	testRoute := routes.GetByID(9999)
	if testRoute != nil {
		t.Errorf("Expected nil route but got non-nil.")
	}
	testRoute = routes.GetByID(testID)
	if testRoute.ID != route.ID {
		t.Errorf("Expected %v but got %v", route.ID, testRoute.ID)
	}
}
