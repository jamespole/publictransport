package busnetwork

import "time"

// Service describes a service.
type Service struct {
	Delay     int
	StopRoute *StopRoute
	Time      time.Time
	Operated  bool
}

// Services is a collection of pointers to Service(s).
type Services []*Service
