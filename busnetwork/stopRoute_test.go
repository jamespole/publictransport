package busnetwork

import "testing"

func TestGetStopRouteByRouteID(t *testing.T) {
	var testStopID = 1234
	var stop = Stop{
		ID: testStopID,
	}
	var testRouteID = 2345
	var route = Route{
		ID: testRouteID,
	}
	var stopRoute = StopRoute{
		Route: &route,
	}
	stop.StopRoutes = append(stop.StopRoutes, &stopRoute)
	testStopRoute := stop.StopRoutes.GetByRouteID(9999)
	if testStopRoute != nil {
		t.Errorf("Expected nil StopRoute but got non-nil")
	}
	testStopRoute = stop.StopRoutes.GetByRouteID(testRouteID)
	if testStopRoute == nil {
		t.Errorf("Expected non-nil but got nil")
		t.FailNow()
	}
	if testStopRoute.Route.ID != testRouteID {
		t.Errorf("Expected %v but got %v", testRouteID, testStopRoute.Route.ID)
		t.FailNow()
	}
}
