package busnetwork

import "testing"

func TestGetStopByID(t *testing.T) {
	testID := 1234
	stop := Stop{
		ID: testID,
	}
	stops := Stops{}
	stops = append(stops, &stop)
	testStop := stops.GetByID(9999)
	if testStop != nil {
		t.Errorf("Expected nil stop but got non-nil.")
	}
	testStop = stops.GetByID(testID)
	if testStop.ID != stop.ID {
		t.Errorf("Expected %v but got %v", stop.ID, testStop.ID)
	}
}
