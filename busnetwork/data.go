// Package busnetwork contain types that can be used to describe a bus network.
package busnetwork

// Data is a container for data about a bus network.
type Data struct {
	Routes Routes
	Stops  Stops
}
