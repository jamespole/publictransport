package busnetwork

// StopRoute describes a route passing through a stop. Services are associated
// with a stop and a route through this type. This allows direct retrieval of
// services along a specific route stopping at a specific stop.
type StopRoute struct {
	Stop     *Stop
	Route    *Route
	Services Services
}

// StopRoutes is a collection of pointers to StopRoute(s).
type StopRoutes []*StopRoute

// GetByRouteID returns the stopRoute with the given routeID within stopRoutes.
func (stopRoutes StopRoutes) GetByRouteID(routeID int) *StopRoute {
	for _, stopRoute := range stopRoutes {
		if stopRoute.Route.ID == routeID {
			return stopRoute
		}
	}
	return nil
}
