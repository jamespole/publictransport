package buscheckweb

import (
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

const SUFFIX_HTML_DOCUMENT = ".html"
const SUFFIX_HTML_TEMPLATE = ".thtml"

// Generate prints HTML code to standard output
func Generate(data busnetwork.Data, templateDirectory string,
	outputDirectory string) error {
	templateDirectoryContents, err := ioutil.ReadDir(templateDirectory)
	if err != nil {
		return err
	}
	for _, file := range templateDirectoryContents {
		if strings.HasSuffix(file.Name(), SUFFIX_HTML_TEMPLATE) != true {
			continue
		}
		templateFilePath := filepath.Join(templateDirectory,
			file.Name())
		template, err := template.ParseFiles(templateFilePath)
		if err != nil {
			return err
		}
		outputFileName := strings.TrimSuffix(file.Name(), SUFFIX_HTML_TEMPLATE)
		outputFileName += SUFFIX_HTML_DOCUMENT
		outputFilePath := filepath.Join(outputDirectory, outputFileName)
		file, err := os.Create(outputFilePath)
		if err != nil {
			return err
		}
		err = template.Execute(file, data)
		if err != nil {
			return err
		}
	}
	return nil
}
