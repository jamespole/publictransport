package auckland

import (
	"errors"
	"fmt"

	"github.com/tealeg/xlsx"
)

type headings struct {
	actualDeparture          int
	actualDepartureExists    bool
	date                     int
	dateExists               bool
	routeID                  int
	routeIDExists            bool
	scheduledDeparture       int
	scheduledDepartureExists bool
	startTime                int
	startTimeExists          bool
	stopID                   int
	stopIDExists             bool
}

// checkHeadings checks for mandatory headings and returns an error if any are
// missing.
func checkHeadings(headings headings) error {
	if headings.actualDepartureExists == false {
		return errors.New("Actual Departure heading missing")
	} else if headings.dateExists == false {
		return errors.New("Date heading missing")
	} else if headings.routeIDExists == false {
		return errors.New("Route ID heading missing")
	} else if headings.scheduledDepartureExists == false {
		return errors.New("Scheduled departure heading missing")
	} else if headings.stopIDExists == false {
		return errors.New("Stop ID heading missing")
	}
	return nil
}

func parseHeading(headings *headings, column int, name string) error {
	if name == "Actual departure at this bus stop" {
		headings.actualDeparture = column
		headings.actualDepartureExists = true
	} else if name == "Bus Stop Description" {
		// do nothing
	} else if name == "Bus Stop Id" {
		headings.stopID = column
		headings.stopIDExists = true
	} else if name == "Bus Stop Id Public" {
		headings.stopID = column
		headings.stopIDExists = true
	} else if name == "Departure Sighting Time" {
		headings.actualDeparture = column
		headings.actualDepartureExists = true
	} else if name == "Display Route Number" {
		// do nothing
	} else if name == "Route Destination" {
		// do nothing
	} else if name == "Route Name" {
		// do nothing
	} else if name == "Route Origin" {
		// do nothing
	} else if name == "Route Uid" {
		headings.routeID = column
		headings.routeIDExists = true
	} else if name == "Scheduled departure at this bus stop" {
		headings.scheduledDeparture = column
		headings.scheduledDepartureExists = true
	} else if name == "Service Date" {
		headings.date = column
		headings.dateExists = true
	} else if name == "Service Start Time (Hh24mm)" {
		headings.startTime = column
		headings.startTimeExists = true
	} else if name == "Timetable Arrival Time" {
		headings.scheduledDeparture = column
		headings.scheduledDepartureExists = true
	} else {
		return fmt.Errorf("Heading %v not recognised", name)
	}
	return nil
}

func parseHeadings(row *xlsx.Row) (headings, error) {
	var headings headings
	for column, heading := range row.Cells {
		name, err := heading.String()
		if err != nil {
			return headings, err
		}
		err = parseHeading(&headings, column, name)
		if err != nil {
			return headings, err
		}
	}
	return headings, checkHeadings(headings)
}
