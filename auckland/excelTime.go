package auckland

import (
	"bytes"
	"time"

	"github.com/tealeg/xlsx"
)

func parseTime(inputDate *xlsx.Cell, inputTime *xlsx.Cell, format string) (time.Time, error) {
	var output time.Time
	dateString, err := inputDate.String()
	if err != nil {
		return output, err
	}
	timeString, err := inputTime.String()
	if err != nil {
		return output, err
	}
	var buffer bytes.Buffer
	buffer.WriteString(dateString)
	buffer.WriteString(" ")
	buffer.WriteString(timeString)
	output, err = time.Parse(format, buffer.String())
	if err != nil {
		return output, err
	}
	return output, nil
}
