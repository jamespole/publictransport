package auckland

import (
	"bitbucket.org/jamespole/publictransport/busnetwork"
	"github.com/tealeg/xlsx"
)

// ParseFile parses the file at the given path and merges parsed data into the
// given data. It is assumed the given data has stops already populated.
func ParseFile(data *busnetwork.Data, path string) error {
	file, err := xlsx.OpenFile(path)
	if err != nil {
		return err
	}
	for _, sheet := range file.Sheets {
		err = parseSheet(data, sheet)
		if err != nil {
			return err
		}
	}
	return nil
}

func parseRows(data *busnetwork.Data, rows []*xlsx.Row, headings headings) error {
	for rowID, row := range rows {
		if rowID == 0 {
			continue
		}
		record, err := parseRow(row, headings)
		if err != nil {
			return err
		}
		build(data, record)
	}
	return nil
}

func parseSheet(data *busnetwork.Data, sheet *xlsx.Sheet) error {
	if len(sheet.Rows) <= 1 {
		return nil
	}
	headings, err := parseHeadings(sheet.Rows[0])
	if err != nil {
		return err
	}
	err = parseRows(data, sheet.Rows, headings)
	if err != nil {
		return err
	}
	return nil
}
