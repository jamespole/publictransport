package auckland

import (
	"encoding/csv"
	"fmt"
	"io"
	"regexp"
	"os"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

func ParseCSVFile (data *busnetwork.Data, path string) error {
	csvFile, err := os.Open(path)
	if err != nil {
		return err
	}
	defer csvFile.Close()
	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1 // Accept any number of fields per record
	dateRegex := regexp.MustCompile(`^([1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/([0-9]|[1-9][0-9]+)$`)
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if len(record) < 9 {
			continue
		}
		if dateRegex.MatchString(record[0]) == false {
			return fmt.Errorf("Error parsing date field, found '%q'", record[0])
		}
	}
	return nil
}
