package auckland

import (
	"fmt"
	"time"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

type record struct {
	actualDeparture    time.Time
	operated           bool
	routeID            int
	scheduledDeparture time.Time
	stopID             int
}

func build(data *busnetwork.Data, record record) error {
	stop := data.Stops.GetByID(record.stopID)
	if stop == nil {
		return fmt.Errorf("auckland: Stop %v not found", record.stopID)
	}
	route := data.Routes.GetByID(record.routeID)
	if route == nil {
		return fmt.Errorf("auckland: Route %v not found",
			record.routeID)
	}
	var stopRoute = buildStopRoute(data, record, route, stop)
	var _ = buildService(data, record, stopRoute)
	return nil
}

func buildService(data *busnetwork.Data, record record,
	stopRoute *busnetwork.StopRoute) *busnetwork.Service {
	var service = busnetwork.Service{
		StopRoute: stopRoute,
		Time:      record.scheduledDeparture,
	}
	if record.operated == true {
		service.Delay = int(record.actualDeparture.Sub(record.scheduledDeparture).Seconds())
		service.Operated = true
	}
	stopRoute.Services = append(stopRoute.Services, &service)
	stopRoute.Stop.Services = append(stopRoute.Stop.Services, &service)
	return &service
}

func buildStopRoute(data *busnetwork.Data, record record, route *busnetwork.Route, stop *busnetwork.Stop) *busnetwork.StopRoute {
	var stopRoute = stop.StopRoutes.GetByRouteID(route.ID)
	if stopRoute != nil {
		return stopRoute
	}
	var newStopRoute = busnetwork.StopRoute{
		Route: route,
		Stop:  stop,
	}
	stop.StopRoutes = append(stop.StopRoutes, &newStopRoute)
	route.StopRoutes = append(route.StopRoutes, &newStopRoute)
	return &newStopRoute
}
