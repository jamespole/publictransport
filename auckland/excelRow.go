package auckland

import (
	"time"

	"github.com/tealeg/xlsx"
)

func parseRow(row *xlsx.Row, headings headings) (record, error) {
	var err error
	var record record
	// Process scheduledDeparture first since actualDeparture needs this.
	record.scheduledDeparture, err =
		parseTime(row.Cells[headings.date],
			row.Cells[headings.scheduledDeparture],
			"2\\/01\\/2006 15:04")
	if err != nil {
		return record, err
	}
	timeString, err := row.Cells[headings.actualDeparture].String()
	if err != nil {
		return record, err
	}
	if timeString != "No GPS data available" &&
		timeString != "No GPS data found" {
		record.actualDeparture, err =
			parseTime(row.Cells[headings.date],
				row.Cells[headings.actualDeparture],
				"2\\/01\\/2006 15:04:05")
		if err != nil {
			return record, err
		}
		// If scheduledDeparture is in the afternoon/evening and
		// actualDeparture is in the morning, then adjust the time so
		// actualDeparture is the next day. When a service scheduled to
		// depart in the evening (e.g. 23:55) actually departs in the
		// morning of the next day (e.g. 00:05) this is recorded by AT
		// as occuring on the scheduled day, which this code corrects.
		if record.scheduledDeparture.Hour() >= 12 &&
			record.actualDeparture.Hour() < 12 {
			record.actualDeparture =
				record.actualDeparture.Add(time.Hour * 24)
		}
		record.operated = true
	}
	record.routeID, err = row.Cells[headings.routeID].Int()
	if err != nil {
		return record, err
	}
	record.stopID, err = row.Cells[headings.stopID].Int()
	if err != nil {
		return record, err
	}
	return record, nil
}
