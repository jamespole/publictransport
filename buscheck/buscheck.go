package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/jamespole/publictransport/auckland"
	"bitbucket.org/jamespole/publictransport/buscheckweb"
	"bitbucket.org/jamespole/publictransport/busnetwork"
	"bitbucket.org/jamespole/publictransport/gtfs"
)

func main() {
	csvFile := flag.String("c", "", "CSV file")
	excelFile := flag.String("e", "", "Excel file")
	gtfsFile := flag.String("g", "", "GTFS file")
	templates := flag.String("t", "", "Template directory")
	output := flag.String("o", "", "Output directory")
	flag.Parse()
	if (*csvFile == "" && *gtfsFile == "" && *excelFile == "") || *output == "" {
		flag.Usage()
		os.Exit(1)
	}
	if *templates == "" {
		*templates = *output
	}
	var data busnetwork.Data
	if *gtfsFile != "" {
		err := gtfs.ParseFile(&data, *gtfsFile)
		if err != nil {
			fmt.Printf("Error parsing GTFS file: %s\n", *gtfsFile)
			fmt.Println(err)
			os.Exit(4)
		}
	}
	if *excelFile != "" {
		err := auckland.ParseFile(&data, *excelFile)
		if err != nil {
			fmt.Printf("Error parsing Excel file: %v\n", *excelFile)
			fmt.Println(err)
			os.Exit(2)
		}
	}
	if *csvFile != "" {
		err := auckland.ParseCSVFile(&data, *csvFile)
		if err != nil {
			fmt.Printf("Error parsing CSV file: %q\n", *csvFile)
			fmt.Println(err)
			os.Exit(5)
		}
	}
	err := buscheckweb.Generate(data, *templates, *output)
	if err != nil {
		fmt.Println("Error generating output:")
		fmt.Printf("  Input from: %v\n", *templates)
		fmt.Printf("  Output to: %v\n", *output)
		fmt.Println(err)
		os.Exit(3)
	}
	os.Exit(0)
}
