James Pole's Public Transport Golang Code Repository
====================================================

Introduction
------------

I have a keen interest in analysing the performance of the public transport
services in the city I live in (Auckland, New Zealand). As a result I have
coded some golang packages to help with my analysis of raw data provided by our
public transport agency (Auckland Transport).

Packages
--------

The *busnetwork* package defines the data structure I use to analyse the
Auckland bus network. It currently defines sub-structures for locations, routes,
services and stops. Input data can be parsed and added into this structure.
Output data is generated from this structure. So this structure allows easy
merging of data from various sources into output such as a consolidated report.

The *gtfs* package contains a parser for GTFS (General Transit Feed
Specification, formerly Google Transit Feed Specification) files. Auckland
Transport (the agency) provides information about bus stops in their GTFS feed,
which is parsed and added to a *busnetwork* data structure.

The *auckland* package contains a parser for XLSX (Microsoft Excel XML format)
files supplied by Auckland Transport (the agency). Auckland Transport provides
information about scheduled and actual departure times of bus services, which is
parsed and added to a *busnetwork* data structure.

The *buscheckweb* package contains a generator that transforms *busnetwork* data
structures into output files(s) such as HTML documents. Templates files (using
the *html/template* standard package) are used to generate output files.

The *buscheck* package contains a command-line application which (1) reads in
a GTFS file for bus stop data; (2) reads in a XLSX file for bus service data;
and (3) generates output file(s) based on given template(s) using the given bus
stop and bus services data.

Code Documentation
------------------

See the
[GoDoc website](https://godoc.org/bitbucket.org/jamespole/publictransport)
for code documentation.
