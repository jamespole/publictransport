package gtfs

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

const (
	stopIDColumn   int = 3
	stopNameColumn int = 6
)

func parseStops(data *busnetwork.Data, f io.Reader) error {
	csvReader := csv.NewReader(f)
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		} else if record[stopIDColumn] == "stop_id" {
			continue
		}
		var stop busnetwork.Stop
		stop.ID, err = strconv.Atoi(record[stopIDColumn])
		if err != nil {
			continue
		} else if data.Stops.GetByID(stop.ID) != nil {
			return fmt.Errorf("Duplicate stop %v", stop.ID)
		}
		stop.Name = record[stopNameColumn]
		data.Stops = append(data.Stops, &stop)
	}
	return nil
}
