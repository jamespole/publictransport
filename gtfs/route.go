package gtfs

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

const (
	routeLongNameColumn  int = 0
	routeTypeColumn      int = 1
	routeIDColumn        int = 4
	routeShortNameColumn int = 6

	routeTypeBus int = 3
)

func parseRoutes(data *busnetwork.Data, f io.Reader) error {
	csvReader := csv.NewReader(f)
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		} else if record[routeIDColumn] == "route_id" {
			continue
		}
		var route busnetwork.Route
		route.ID, err = strconv.Atoi(record[routeIDColumn][0:5])
		if err != nil {
			continue
		}
		route.LongName = record[routeLongNameColumn]
		route.ShortName = record[routeShortNameColumn]
		// Check for duplicate routes with the same route ID. If it has
		// the same short name and long name, then ingore it.
		duplicateRoute := data.Routes.GetByID(route.ID)
		if duplicateRoute != nil &&
			duplicateRoute.ShortName != route.ShortName &&
			duplicateRoute.LongName != route.LongName {
			return fmt.Errorf("Duplicate route %v", route.ID)
		}
		data.Routes = append(data.Routes, &route)
	}
	return nil
}
