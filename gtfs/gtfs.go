// Package gtfs contains a parser that parses GTFS files into a busnetwork
// struct.
package gtfs

import (
	"archive/zip"

	"bitbucket.org/jamespole/publictransport/busnetwork"
)

// ParseFile parses the GTFS file with the given path and merges parsed data
// into the given data.
func ParseFile(data *busnetwork.Data, path string) error {
	r, err := zip.OpenReader(path)
	defer r.Close()
	if err != nil {
		return err
	}
	for _, f := range r.File {
		file, err := f.Open()
		if err != nil {
			return err
		}
		if f.Name == "stops.txt" {
			err = parseStops(data, file)
		} else if f.Name == "routes.txt" {
			err = parseRoutes(data, file)
		}
		if err != nil {
			return err
		}
	}
	return nil
}
